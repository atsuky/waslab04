
function showSongs () {
   var content = document.getElementById("content").value;

   // Replace the two lines below with your implementation   
   SONGS_URI = "http://localhost:8080/waslab04/lyric_search.php"
   
	var uri = SONGS_URI;
	
	req = new XMLHttpRequest();
	req.open('GET', uri + "?" + "song_text=" + content, true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
            json = JSON.parse(req.responseText);
            html_out = ""
            for (var i in json) {
                if (json[i] != null){
                    html_out += '<a href="javascript:getLyric(\''+ json[i].LyricId +'\',\'' + json[i].LyricChecksum + '\');" >' + json[i].Song + '</a> (' + json[i].Artist + ')<br><br>';
                }
            }
			document.getElementById("left").innerHTML = html_out;
		}
	};
	req.send(null);
   
};


function getLyric (song_id, song_checksum) {
   console.log(song_id);
   console.log(song_checksum);
   
   LYRICS_URI = "http://localhost:8080/waslab04/get_lyric.php"
   var uri = LYRICS_URI + "?" + "song_id=" + song_id + "&song_checksum=" + song_checksum;
   console.log(uri);
   req = new XMLHttpRequest();
	req.open('GET', uri , true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
            json = JSON.parse(req.responseText);
            songInfo = json[0]["GetLyricResult"];
            lyrics = songInfo.Lyric.replace(/(?:\r\n|\r|\n)/g, '<br>');
            html_out=   "<div class=\"row\">" +
                        "  <div class=\"col\" id=\"song-name\"><b><h2>" + songInfo.LyricSong + "</h2><h3>" + songInfo.LyricArtist +  "</h3></b></div>" +
                        "  <div class=\"col\" id=\"image\"> <img src=\" " + songInfo.LyricCovertArtUrl + " \"></div>" +
                        "</div>" +
                        "<div class=\"row\">" +
                        "  <div class=\"col\" id=\"lyrics\"> " + lyrics + " </div>" +
                        "</div>";

            
            
            document.getElementById("right").innerHTML = html_out;
		}
	};
	req.send(null);
   
}

window.onload = showSongs();