<?php
ini_set("soap.wsdl_cache_enabled","0");
header('Content-Type: application/json');

try{

  $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');

    // Get the necessary parameters from the request
    // Use $sClient to call the operation SearchLyricText
    // echo the returned info as a JSON array of objects
    
    //header(':', true, 501); // Just remove this line to return the successful 
                          // HTTP-response status code 200.
                          
    $text = $_GET["song_text"];
    
    $api_req_obj = new stdClass();
    $api_req_obj->lyricText = $text;
    
    
    $result = $sClient->SearchLyricText($api_req_obj);
    
    $data = json_encode((array)$result);
    $array = json_decode($data,TRUE);
    echo json_encode($array['SearchLyricTextResult']['SearchLyricResult']);
  
}
catch(SoapFault $e){
  header(':', true, 500);
  echo json_encode($e);
}
?>
