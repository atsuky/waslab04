<?php
ini_set("soap.wsdl_cache_enabled","0");
header('Content-Type: application/json');

try{

  $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');

    // Get the necessary parameters from the request
    // Use $sClient to call the operation GetLyric
    // echo the returned info as a JSON object
    $sid = $_GET["song_id"];
    $scheck = $_GET["song_checksum"];
    
    $api_req_obj = new stdClass();
    $api_req_obj->lyricId = $sid;
    $api_req_obj->lyricCheckSum = $scheck;
    
    $result = $sClient->GetLyric($api_req_obj);
    
    echo json_encode(array($result));

}
catch(SoapFault $e){
  header(':', true, 500);
  echo json_encode($e);
}

